using NUnit.Framework;
using Maiores_ou_menores;

namespace MaiorMenorUnit
{
    public class MaiorMenorUnit
    {
        [SetUp]
        public void Setup()
        {
        }

        [TestCase(1, 2, 3)]
        [TestCase(1, 3, 2)]
        [TestCase(2, 3, 1)]
        [TestCase(2, 1, 3)]
        [TestCase(3, 1, 2)]
        [TestCase(3, 2, 1)]
        public void Maior_QuandoExecutadaComNumerosDiferentes_RetornaMaiorNumero(int a, int b, int c)
        {
            // arrange
            var maiorOuMenor = new Maiores_ou_menores();
            maiorOuMenor.Num1 = a;
            maiorOuMenor.Num2 = b;
            maiorOuMenor.Num3 = c;

            // act 
            var resultado = maiorOuMenor.Maior();

            //assert
            Assert.That(resultado, Is.EqualTo(3));
        }

        [TestCase(1, 1, 2, 2)]
        [TestCase(1, 2, 1, 2)]
        [TestCase(2, 1, 1, 2)]
        public void Maior_QuandoExecutadaComDoisNumeroIguais_RetornaMaiorNumero(int a, int b, int c, int esperado)
        {
            // arrange
            var maiorOuMenor = new Maiores_ou_menores();
            maiorOuMenor.Num1 = a;
            maiorOuMenor.Num2 = b;
            maiorOuMenor.Num3 = c;

            // act 
            var resultado = maiorOuMenor.Maior();

            //assert
            Assert.That(resultado, Is.EqualTo(esperado));
        }

        [TestCase(1, 1, 1, 1)]
        public void Maior_QuandoExecutadaComTodosOsNumeroIguais_RetornaMaiorNumero(int a, int b, int c, int esperado)
        {
            // arrange
            var maiorOuMenor = new Maiores_ou_menores();
            maiorOuMenor.Num1 = a;
            maiorOuMenor.Num2 = b;
            maiorOuMenor.Num3 = c;

            // act 
            var resultado = maiorOuMenor.Maior();

            //assert
            Assert.That(resultado, Is.EqualTo(esperado));
        }

        /*********** Function Menor *********/

        [TestCase(1, 2, 3)]
        [TestCase(1, 3, 2)]
        [TestCase(2, 3, 1)]
        [TestCase(2, 1, 3)]
        [TestCase(3, 1, 2)]
        [TestCase(3, 2, 1)]
        public void Menor_QuandoExecutadaComNumerosDiferentes_RetornaMenorNumero(int a, int b, int c)
        {
            // arrange
            var maiorOuMenor = new Maiores_ou_menores();
            maiorOuMenor.Num1 = a;
            maiorOuMenor.Num2 = b;
            maiorOuMenor.Num3 = c;

            // act 
            var resultado = maiorOuMenor.Menor();

            //assert
            Assert.That(resultado, Is.EqualTo(1));
        }

        [TestCase(1, 1, 2, 1)]
        [TestCase(1, 2, 2, 1)]
        [TestCase(1, 2, 2, 1)]
        [TestCase(2, 2, 1, 1)]
        public void Menor_QuandoExecutadaComDoisNumeroIguais_RetornaMenorNumero(int a, int b, int c, int esperado)
        {
            // arrange
            var maiorOuMenor = new Maiores_ou_menores();
            maiorOuMenor.Num1 = a;
            maiorOuMenor.Num2 = b;
            maiorOuMenor.Num3 = c;

            // act 
            var resultado = maiorOuMenor.Menor();

            //assert
            Assert.That(resultado, Is.EqualTo(esperado));
        }

        [TestCase(1, 1, 1, 1)]
        public void Menor_QuandoExecutadaComTodosOsNumeroIguais_RetornaMenorNumero(int a, int b, int c, int esperado)
        {
            // arrange
            var maiorOuMenor = new Maiores_ou_menores();
            maiorOuMenor.Num1 = a;
            maiorOuMenor.Num2 = b;
            maiorOuMenor.Num3 = c;

            // act 
            var resultado = maiorOuMenor.Menor();

            //assert
            Assert.That(resultado, Is.EqualTo(esperado));
        }
    }
}