using NUnit.Framework;
using Calculadora;

namespace Calculadora.UnitTests
{
    public class CalculadoraTests
    {
        [SetUp]
        public void Setup()
        {

        }

        [Test]
        public void Total_QuandoAcaoForSomar_RetornaSoma()
        {

            //arrange
            var calculadora = new calculo();
            calculadora.campo1 = 1;
            calculadora.campo2 = 2;
            calculadora.tipodecalculo = "+";



            //act
            var resultado = calculadora.Total();

            //assert

            Assert.That(resultado, Is.EqualTo(3));

            Assert.Pass();
        }
        [Test]
        public void Total_QuandoAcaoForSubtrair_RetornaSub()
        {

            var calculadora = new calculo();
            calculadora.campo1 = 5;
            calculadora.campo2 = 2;
            calculadora.tipodecalculo = "-";

            var resultado = calculadora.Total();

            Assert.That(resultado, Is.EqualTo(3));

            Assert.Pass();


        }
   
     
    }





}
    